import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useLoginStore = defineStore("login", () => {
  const name = ref("");
  const isLogin = computed(() => {
    // name is not empty
    return name.value !== "";
  });

  const login = (userName: string): void => {
    name.value = userName;
    localStorage.setItem("name", userName);
  };
  const logout = () => {
    name.value = "";
    localStorage.removeItem("name");
  };
  const loadData = () => {
    name.value = localStorage.getItem("name") || "";
  };

  return { name, isLogin, login, logout, loadData };
});
